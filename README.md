# Stock Importer

This project is used to import data from Yahoo finance to database.

![Project] (images/progress.jpg)  

Thanks to Brad Lucas's project from:
 - [this] (http://blog.bradlucas.com/posts/2017-06-03-yahoo-finance-quote-download-python/) and 
 - [this] (https://github.com/bradlucas/get-yahoo-quotes-python)