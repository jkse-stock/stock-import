import datetime

import cymysql
import dateutil.relativedelta
import pandas as pd


def draw_month(stock):
    gain_stmt = """
                SELECT COUNT(*) AS rate 
                FROM history h, stock s
                WHERE h.s_close > h.s_open
                    AND h.s_id = s.s_id
                    AND h.s_low <> h.s_high
                    AND s.s_quote = '%s'
                    AND MONTH(h.s_date) = %d
                    AND YEAR(h.s_date) = %d
                """

    lose_stmt = """
                SELECT COUNT(*) AS rate 
                FROM history h, stock s
                WHERE h.s_close < h.s_open
                    AND h.s_id = s.s_id
                    AND h.s_low <> h.s_high
                    AND s.s_quote = '%s'
                    AND MONTH(h.s_date) = %d
                    AND YEAR(h.s_date) = %d
                """

    neutral_stmt = """
                SELECT COUNT(*) AS rate 
                FROM history h, stock s
                WHERE h.s_close-h.s_open = 0
                    AND h.s_id = s.s_id
                    AND h.s_low <> h.s_high
                    AND s.s_quote = '%s'
                    AND MONTH(h.s_date) = %d
                    AND YEAR(h.s_date) = %d
                """

    date = """
                SELECT YEAR(MIN(h.s_date)), MONTH(MIN(h.s_date))
                FROM history h, stock s
                WHERE h.s_id = s.s_id
                AND s.s_quote = '%s'
                """

    conn = cymysql.connect(host='127.0.0.1', user='stock', passwd='stock', db='stock', charset='utf8')
    cur = conn.cursor()
    cur.execute(date % stock)
    date_part = cur.fetchone()

    first = datetime.date(date_part[0], 1, 1)
    last = datetime.date.today()
    last = datetime.date(last.year, 12, 31)
    current = first

    labels = ['year', 'month', 'gain', 'lose', 'neutral']
    data = []

    while current <= last:
        cur.execute(gain_stmt % (stock, current.month, current.year))
        gain_count, = cur.fetchone()
        cur.execute(lose_stmt % (stock, current.month, current.year))
        lose_count, = cur.fetchone()
        cur.execute(neutral_stmt % (stock, current.month, current.year))
        neutral_count, = cur.fetchone()
        # print("%s, %s, %d, %d, %d" % (current.year, current.month, gain_count, lose_count, neutral_count))
        data.append([current.year, current.month, gain_count, lose_count, neutral_count])
        current += dateutil.relativedelta.relativedelta(months=1)

    data = pd.DataFrame.from_records(data=data, columns=labels)
    # print(data.head(5))

    cur.close()
    conn.close()

    return data


def recurring(data, last_date='0'):
    if last_date == '0':
        with cymysql.connect(host='127.0.0.1', user='stock', passwd='stock', db='stock', charset='utf8') as cur:
            cur.execute("""SELECT MAX(s_date) FROM history""")
            last_date, = cur.fetchone()
            last_date = last_date.strftime("%Y-%m-%d")
            cur.close()

    rec_data = pd.concat(i for _, i in data.groupby('stock') if len(i) > 1)

    for stock in rec_data[rec_data['date'] == last_date].itertuples():
        print(rec_data[rec_data['stock'] == stock.stock])


def get(__date=datetime.date.today(), months=0, days=0):
    date_format = "%Y-%m-%d"
    __prev = __date + dateutil.relativedelta.relativedelta(months=-months, days=-days)

    conn = cymysql.connect(host='127.0.0.1', user='stock', passwd='stock', db='stock', charset='utf8')

    cur = conn.cursor()
    gainers_stmt = """
                SELECT s.s_quote, h.s_date, h.s_open, h.s_close, h.s_high, h.s_low, (h.s_close/h.s_open-1) AS rate
                FROM history h, stock s
                WHERE h.s_id = s.s_id 
                    AND h.s_date = '%s'
                GROUP BY h.s_id, h.s_date
                ORDER BY rate DESC
                LIMIT 10
              """
    losers_stmt = """
                SELECT s.s_quote, h.s_date, h.s_open, h.s_close, h.s_high, h.s_low, (h.s_close/h.s_open-1) AS rate
                FROM history h, stock s
                WHERE h.s_id = s.s_id 
                    AND h.s_date = '%s' 
                GROUP BY h.s_id, h.s_date
                ORDER BY rate ASC 
                LIMIT 10
              """

    gainers = pd.DataFrame(index=['date', 'stock', 'open', 'close', 'high', 'low', 'rate'])
    losers = pd.DataFrame(index=['date', 'stock', 'open', 'close', 'high', 'low', 'rate'])

    __current = __prev
    while __current <= __date:
        cur.execute(gainers_stmt % __current.strftime(date_format))
        for row in cur.fetchall():
            if row[4] != 0.0:
                gainers = gainers.append({
                    'date': pd.to_datetime(row[1]),
                    'open': row[2],
                    'close': row[3],
                    'high': row[4],
                    'low': row[5],
                    'stock': row[0],
                    'rate': row[6]
                }, ignore_index=True)

        cur.execute(losers_stmt % __current.strftime(date_format))
        for row in cur.fetchall():
            if row[4] != 0.0:
                losers = losers.append({
                    'date': pd.to_datetime(row[1]),
                    'open': row[2],
                    'close': row[3],
                    'high': row[4],
                    'low': row[5],
                    'stock': row[0],
                    'rate': row[6]
                }, ignore_index=True)

        __current += datetime.timedelta(days=1)

    gainers = gainers.dropna()
    losers = losers.dropna()

    cur.close()
    conn.close()

    return gainers, losers


if __name__ == '__main__':
    main(days=5)
    # for i in range(1, 29):
    #     cur.execute(gainers_stmt % i)
    #     for row in cur.fetchall():
    #         if row[4] != 0.0:
    #             gainers = gainers.append({
    #                 'date': row[1],
    #                 'stock': row[0],
    #                 'rate': row[4]
    #             }, ignore_index=True)
    #
    # for i in range(1, 29):
    #     cur.execute(losers_stmt % i)
    #     for row in cur.fetchall():
    #         if row[4] != 0.0:
    #             losers = losers.append({
    #                 'date': row[1],
    #                 'stock': row[0],
    #                 'rate': row[4]
    #             }, ignore_index=True)
    #
    # gainers = gainers.dropna()
    # losers = losers.dropna()

    # print(pd.merge(losers, gainers, how='inner', on=['stock']))

    # print(losers.head(5))

    # LOSERS COME AGAIN
    # for __date in losers.groupby('date').agg(lambda x: ','.join(x)).index:
    #     print(losers.loc[losers['date'] == __date])
    #     print(gainers.loc[gainers['date'] == __date])
    #     for data in losers.loc[losers['date'] == __date].itertuples():
    #         recurred = gainers.loc[gainers.stock == data.stock]
    #         if not recurred.empty:
    #             for __any in recurred.itertuples():
    #                 if __any.date > __date:
    #                     print(
    #                         "%s\n"
    #                         "last: %s\n"
    #                         "last: %.0f%%\n"
    #                         "next: %s\n"
    #                         "next: %.0f%%\n"
    #                         "days: %s\n"
    #                         % (data.stock, __date, data.rate * 100, __any.date, __any.rate * 100, __any.date - __date))
    #                     input("####")
    #     input('%s' % __date)

    # WINNERS TAKES ALL
    # for __date in gainers.groupby('date').agg(lambda x: ','.join(x)).index:
    #     # print(losers.loc[losers['date'] == name])
    #     # print(gainers.loc[gainers['date'] == name])
    #     for data in gainers.loc[gainers['date'] == __date].itertuples():
    #         recurred = gainers.loc[gainers.stock == data.stock]
    #         if not recurred.empty:
    #             for __any in recurred.itertuples():
    #                 if __any.date > __date:
    #                     print(
    #                         "%s\n"
    #                         "last: %s\n"
    #                         "last: %.0f%%\n"
    #                         "next: %s\n"
    #                         "next: %.0f%%\n"
    #                         "days: %s\n"
    #                         % (data.stock, __date, data.rate * 100, __any.date, __any.rate * 100, __any.date - __date))
    #                     input("####")
    #     input('%s' % __date)
