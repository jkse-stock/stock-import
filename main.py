import os
import time
from multiprocessing.pool import Pool

import cymysql
import pandas as pd
from cymysql import DataError, IntegrityError

import YahooQuotes as Yahoo

folder = './stock'


def main():
    conn = cymysql.connect(host='127.0.0.1', user='stock', passwd='stock', db='stock', charset='utf8')

    cur = conn.cursor()
    cur.execute("""SELECT s_quote FROM stock WHERE s_quote NOT LIKE '%-W'""")

    # print("##### DOWNLOAD #####")
    # download_file(cur)
    cur.close()

    print("#### INSERT ####")
    insert_stock(conn)

    cur.close()
    conn.close()


def download_file(cursor):
    stocks = [stock[0] + '.JK' for stock in cursor]
    print(len(stocks))

    pool = Pool(processes=10)
    for stock in stocks:
        pool.apply_async(Yahoo.download, args=(stock, folder))
    pool.close()
    pool.join()


def insert_stock(connection):
    cur = connection.cursor()
    stmt = """
            INSERT INTO history 
            (s_id, s_open, s_high, s_low, s_close, s_adj_close, s_volume, s_date) 
            SELECT s_id,%s,%s,%s,%s,%s,%s,'%s' FROM stock WHERE s_quote = '%s'
            """

    files = os.listdir(folder)

    for file in files:
        print("FILE: %s\t" % file, end='')
        start = time.time()
        stocks = pd.read_csv("%s/%s" % (folder, file))
        if stocks.empty:
            print("EMPTY")
            break

        stocks = stocks.dropna()
        stocks.columns = ['s_date', 's_open', 's_high', 's_low', 's_close', 's_adj_close', 's_volume']
        stocks = stocks[['s_open', 's_high', 's_low', 's_close', 's_adj_close', 's_volume', 's_date']]
        stocks['s_quote'] = file.replace('.JK.csv', '')

        for data in stocks.itertuples():
            try:
                cur.execute(stmt % (
                    data.s_open,
                    data.s_high,
                    data.s_low,
                    data.s_close,
                    data.s_adj_close,
                    data.s_volume,
                    data.s_date,
                    data.s_quote
                ))
            except DataError:
                print(
                    data.s_open,
                    data.s_high,
                    data.s_low,
                    data.s_close,
                    data.s_adj_close,
                    data.s_volume,
                    data.s_date,
                    data.s_quote)
            except IntegrityError:
                continue
        connection.commit()
        end = time.time()
        print("DONE took %0.2f s" % (end-start))

    # for file in os.listdir(folder):
    #     os.remove("%s/%s" % (folder, file))


def import_stock():
    conn = cymysql.connect(host='127.0.0.1', user='stock', passwd='stock', db='stock', charset='utf8')
    stmt = """INSERT INTO stock (s_quote, s_name) VALUES (%s,%s)"""
    stocks = []
    with open('stock_list.csv') as f:
        for lines in f:
            stocks.append((lines.split(';')[1], lines.split(';')[2]))

        cur = conn.cursor()
        cur.executemany(stmt, stocks)
        conn.commit()

        cur.close()
        conn.close()


if __name__ == '__main__':
    main()
    # import_stock()
