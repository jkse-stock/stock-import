import re
import time

import requests


def time2epoch(date):
    pattern = '%Y-%m-%d'
    return int(time.mktime(time.strptime(date, pattern)))


def download(quote, folder, start_date=0, end_date=int(time.time())):
    url = "https://finance.yahoo.com/quote/%s/?p=%s" % (quote, quote)
    r = requests.get(url)
    cookie = {'B': r.cookies['B']}
    crumb = ''
    lines = r.content.decode('unicode-escape').strip().replace('}', '\n').split('\n')
    for l in lines:
        if re.findall(r'CrumbStore', l):
            crumb = l.split(':')[2].strip('"')

    filename = '%s.csv' % quote
    url = "https://query1.finance.yahoo.com/v7/finance/download/" \
          "%s?period1=%s&period2=%s&interval=1d&events=history&crumb=%s" % \
          (quote, start_date, end_date, crumb)
    response = requests.get(url, cookies=cookie)
    with open("%s/%s" % (folder, filename), 'wb') as handle:
        for block in response.iter_content(1024):
            handle.write(block)

    print("%s DONE" % quote)
